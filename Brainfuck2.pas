unit Brainfuck2;

{
	This is a Pascal interpreter for Brainfuck 2, a more powerful and 
	fully featured version of Brainfuck. It utilizes many more slots of 
	memory than the original brainfuck. It arranges the memory it uses
	into a 2-dimensional array and 
	
	The new commands in BF 2 are as follows:
	
	~ - Prints and moves the pointer to the left.
	@ - Dumps the memory to a file as input by you.
	v - Moves the pointer down.
	^ - Moves the pointer up.
	| - Halts the program, will return to terminal.
	
	The bracketed code is depreciated since ` will apply a line of code
	to the entire row and ! will apply it to the entire column.
}

interface

uses		{ Load the CRT module. }
	crt;

type
	Brainfuck = object
		//
		// Memory slot
		//
		memory: array[0..1023, 0..1023] of integer;
		
		//
		// Memory slot locater
		//
		ptr_x: integer;
		ptr_y: integer;
		
		//
		// Initializes all the data. All of it.
		//
		procedure init();
		
		//
		// Parses a line of code.
		//
		procedure parse(code: string);
		
		//
		// Parses a key of code
		//
		procedure parse_key(code: string);
		
		//
		// Dumps memory to a file
		//
		procedure dump_mem();
		
	end;
	
implementation

	//
	// Initializes all the data. All of it. 
	//
	procedure Brainfuck.init();
	var i, j: integer;
	begin
		for i := 0 to 1023 do
			begin
				for j := 0 to 1023 do
					begin
						memory[i, j] := 0;
					end;
			end;
		
		ptr_x := 0;
		ptr_y := 0;
	end;
	
	//
	// Parses a line of code
	//
	procedure Brainfuck.parse(code: string);
	var i: integer;
	begin
		if code[1] = '!' then
			begin
				delete(code, 1, 1);
				
				for i := 1 to 1023 do
					begin
						parse(code);
						ptr_x += 1;
					end;
			end;
			
		if code[1] = '`' then
			begin
				delete(code, 1, 1);
				
				for i := 1 to 1023 do
					begin
						parse(code);
						ptr_y += 1;
					end;
			end;
	
		for i := 1 to length(code) do
			begin
				case code[i] of 
					'>': ptr_x += 1;						// Increment ptr
					'<': ptr_x -= 1;						// Decrement ptr
					'v': ptr_y += 1;						
					'^': ptr_y -= 1;	
					'+': memory[ptr_x, ptr_y] += 1;				// Increment mem
					'-': memory[ptr_x, ptr_y] -= 1;				// Decrement mem
					'.': write(chr(memory[ptr_x, ptr_y] + 65));	// Print memslot
					'@': dump_mem();					// Saves memslot
					',': parse_key(readkey());			// Reads a key
					'c': begin
						ptr_x := 0;
						ptr_y := 0;
						end;
					'|': halt();
				else
						
				end;
			end;
	end;
	
	//
	// Parses a key of data
	//
	procedure Brainfuck.parse_key(code: string);
	begin
		parse(code);
	end;
	
	//
	// Dumps memory
	// 
	procedure Brainfuck.dump_mem();
	var f: file of integer; input: string; i, j: integer;
	begin
		readln(input);
		
		assign(f, input);
		rewrite(f);
		
		for i := 0 to 1023 do
			begin
				for j := 0 to 1023 do
					begin
						write(f, memory[i, j]);
					end;
			end;
			
		close(f);
	end;
	
end.
		
